###################################################################################################################################
#
# scspell-id: 7ac3a090-ff0a-11ee-b612-80ee73e9b8e7
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2023-2024 The DPS8M Development Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###################################################################################################################################

###################################################################################################################################
# shell

SHELL:=/bin/sh

###################################################################################################################################
# common files

COMMON_DIR=src/common
COMMON_SRC=$(strip $(wildcard $(COMMON_DIR)/*.c))
COMMON_OBJ=$(strip $(patsubst %.c, %.o, $(COMMON_SRC)))

MXASCII_COMMON_SRC_TMP=$(strip $(patsubst $(COMMON_DIR)/mxdearc.c,, $(COMMON_SRC)))
MXASCII_COMMON_OBJ_TMP=$(strip $(patsubst $(COMMON_DIR)/mxdearc.o,, $(COMMON_OBJ)))
MXASCII_COMMON_SRC=$(strip $(patsubst $(COMMON_DIR)/mxmseg.c,, $(MXASCII_COMMON_SRC_TMP)))
MXASCII_COMMON_OBJ=$(strip $(patsubst $(COMMON_DIR)/mxmseg.o,, $(MXASCII_COMMON_OBJ_TMP)))

###################################################################################################################################
# mxload files

MXLOAD_DIR=src/mxload
MXLOAD_SRC=$(strip $(wildcard $(MXLOAD_DIR)/*.c))
MXLOAD_OBJ=$(strip $(patsubst %.c, %.o, $(MXLOAD_SRC)))

###################################################################################################################################
# mxarc files

MXARC_DIR=src/mxarc
MXARC_SRC=$(strip $(wildcard $(MXARC_DIR)/*.c))
MXARC_OBJ=$(strip $(patsubst %.c, %.o, $(MXARC_SRC)))

###################################################################################################################################
# mxascii files

MXASCII_DIR=src/mxascii
MXASCII_SRC=$(strip $(wildcard $(MXASCII_DIR)/*.c))
MXASCII_OBJ=$(strip $(patsubst %.c, %.o, $(MXASCII_SRC)))

###################################################################################################################################
# mxforum files

MXFORUM_DIR=src/mxforum
MXFORUM_SRC=$(strip $(wildcard $(MXFORUM_DIR)/*.c))
MXFORUM_OBJ=$(strip $(patsubst %.c, %.o, $(MXFORUM_SRC)))

###################################################################################################################################
# mxmap files

MXMAP_DIR=src/mxmap
MXMAP_SRC=$(strip $(wildcard $(MXMAP_DIR)/*.c))
MXMAP_OBJ=$(strip $(patsubst %.c, %.o, $(MXMAP_SRC)))

###################################################################################################################################
# mxmbx files

MXMBX_DIR=src/mxmbx
MXMBX_SRC=$(strip $(wildcard $(MXMBX_DIR)/*.c))
MXMBX_OBJ=$(strip $(patsubst %.c, %.o, $(MXMBX_SRC)))

###################################################################################################################################
# pr8bit files

PR8BIT_DIR=src/pr8bit
PR8BIT_SRC=$(strip $(wildcard $(PR8BIT_DIR)/*.c))
PR8BIT_OBJ=$(strip $(patsubst %.c, %.o, $(PR8BIT_SRC)))

###################################################################################################################################
# doc files

DOC_DIR=doc

###################################################################################################################################
# target: all

ALL_TARGETS:=mxload mxarc mxascii mxforum mxmap mxmbx pr8bit

.PHONY: all
all: $(ALL_TARGETS)
	-@command -v size > /dev/null 2>&1 && { env OBJECT_MODE=32_64 size $(ALL_TARGETS) 2>/dev/null; } || true
	-@printf '\n%s\n' "Optional: \"$(MAKE) docs\" builds the PDF documentation."

###################################################################################################################################
# system specifics

UNAME_S:=$(shell uname -s 2> /dev/null)

ifneq "$(findstring AIX,$(UNAME_S))" ""
  AIX?=1
endif

###################################################################################################################################
# automatic dependency flags

ifneq "$(findstring suncc,$(CC))" ""
  MMDOPT?=-xMMD
endif

ifneq ($(AIX),1)
  MMDOPT?=-MMD
endif

###################################################################################################################################
# automatic dependencies: mxload

ifneq (,$(wildcard $(MXLOAD_DIR)/*.d))
  include $(wildcard $(MXLOAD_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: mxarc

ifneq (,$(wildcard $(MXARC_DIR)/*.d))
  include $(wildcard $(MXARC_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: mxascii

ifneq (,$(wildcard $(MXASCII_DIR)/*.d))
  include $(wildcard $(MXASCII_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: mxforum

ifneq (,$(wildcard $(MXFORUM_DIR)/*.d))
  include $(wildcard $(MXFORUM_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: mxmap

ifneq (,$(wildcard $(MXMAP_DIR)/*.d))
  include $(wildcard $(MXMAP_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: mxmbx

ifneq (,$(wildcard $(MXMBX_DIR)/*.d))
  include $(wildcard $(MXMBX_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: pr8bit

ifneq (,$(wildcard $(PR8BIT_DIR)/*.d))
  include $(wildcard $(PR8BIT_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: common

ifneq (,$(wildcard $(COMMON_DIR)/*.d))
  include $(wildcard $(COMMON_DIR)/*.d)
endif

###################################################################################################################################
# automatic dependencies: pcc-style

ifneq (,$(wildcard ./*.d))
  include $(wildcard ./*.d)
endif

###################################################################################################################################
# target: clean

.PHONY: clean

clean:
	-$(RM) $(ALL_TARGETS) $(MXLOAD_OBJ) $(MXARC_OBJ) $(MXASCII_OBJ) $(MXFORUM_OBJ) $(MXMAP_OBJ) $(MXMBX_OBJ) $(PR8BIT_OBJ)
	-$(RM) $(COMMON_OBJ)
	-$(RM) $(MXLOAD_DIR)/*.d $(MXARC_DIR)/*.d $(MXASCII_DIR)/*.d $(MXFORUM_DIR)/*.d
	-$(RM) $(MXMAP_DIR)/*.d $(MXMBX_DIR)/*.d $(PR8BIT_DIR)/*.d $(COMMON_DIR)/*.d ./*.d
	-$(RM) compile_commands.json compile_commands.events.json
	-$(RM) -r ./*.dSYM
	-@$(MAKE) -C $(DOC_DIR) clean

###################################################################################################################################
# target: disclean

.PHONY: distclean

distclean: clean
	-$(RM) GPATH GRTAGS GTAGS TAGS tags cscope.*

###################################################################################################################################
# target: doc

.PHONY: doc docs

doc docs:
	$(MAKE) -C $(DOC_DIR)

###################################################################################################################################
# target: mxload

mxload: $(MXLOAD_OBJ) $(COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(COMMON_OBJ) $<

###################################################################################################################################
# target: mxarc

mxarc: $(MXARC_OBJ) $(COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(COMMON_OBJ) $<

###################################################################################################################################
# target: mxascii

mxascii: $(MXASCII_OBJ) $(MXASCII_COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(MXASCII_COMMON_OBJ) $<

###################################################################################################################################
# target: mxforum

mxforum: $(MXFORUM_OBJ) $(COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(COMMON_OBJ) $<

###################################################################################################################################
# target: mxmap

mxmap: $(MXMAP_OBJ) $(COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(COMMON_OBJ) $<

###################################################################################################################################
# target: mxmbx

mxmbx: $(MXMBX_OBJ) $(COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(COMMON_OBJ) $<

###################################################################################################################################
# target: pr8bit

pr8bit: $(PR8BIT_OBJ) $(MXASCII_COMMON_OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(MXASCII_COMMON_OBJ) $<

###################################################################################################################################
# objects: default

%.o: %.c
	$(CC) $(MMDOPT) $(CFLAGS) -I$(COMMON_DIR) -c -o $@ $<

###################################################################################################################################
# debug: printvars

.PHONY: printvars printenv

printvars printenv:
	-@printf '%s: %s\n' "FEATURES" "$(.FEATURES)" 2> /dev/null
	-@$(foreach V,$(sort $(.VARIABLES)),$(if $(filter-out environment% default automatic,$(origin $V)), \
	    $(if $(strip $($V)),$(info $V: [$($V)]),)))
	-@true > /dev/null 2>&1

###################################################################################################################################
# debug: print-%

.PHONY: print-%

print-%:
	-@$(info $*: [$($*)] ($(flavor $*). set by $(origin $*)))@true
	-@true > /dev/null 2>&1

###################################################################################################################################
