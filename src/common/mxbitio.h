/*
 * scspell-id: c2f93236-ff09-11ee-9e76-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !defined(MXBITIO_H_)
# define MXBITIO_H_

# include <stdio.h>

# define INPUT_BUFFER_SIZE 10000

# define MXBITFILE struct mxbitiobuf

extern MXBITFILE *open_mxbit_file(char *name, char *mode);
extern void rewind_mxbit_file(MXBITFILE *mxbitfile, char *mode);
extern void close_mxbit_file(MXBITFILE *mxbitfile);
extern long get_mxbits(MXBITFILE *mxbitfile, long count, char *string);
extern int put_mxbits(MXBITFILE *mxbitfile, int count, char *string);
extern int get_mxstring(MXBITFILE *mxbitfile, char *string, int len);
extern int mxbit_pos(MXBITFILE *mxbitfile, long pos);
extern int get_9_mxbit_integer(MXBITFILE *mxbitfile);
extern int get_36_mxbit_integer(MXBITFILE *mxbitfile, unsigned long *value);
extern long get_18_mxbit_integer(MXBITFILE *mxbitfile);
extern long get_24_mxbit_integer(MXBITFILE *mxbitfile);
extern int skip_mxbits(MXBITFILE *mxbitfile, long n_bits);
extern int eof_reached(MXBITFILE *mxbitfile);

MXBITFILE {
    FILE *realfile;
    unsigned char byte_buffer;
    int byte_buffer_pos;
    long input_file_pos;
    char *buffer_ptr; /* Only used with BIGBUFFER option */
    unsigned char *saved_input_ptr;
    int write;
    int tape_mult_sw;
    int reading_tape_data; /* As opposed to reading header info or non-tape file */
    long n_bytes_left_in_tape_block;
    long n_pad_bytes_in_tape_block;
    int next_tape_block_number;
    int end_of_reel_reached;
    int temp_file;
    char path[16384];
    long reading_buffer_to_pos;
};

extern int getcwrapper(MXBITFILE *mxbitfile);
extern int fseekwrapper(MXBITFILE *mxbitfile, int n);
extern int freadwrapper(unsigned char *ptr, int size, int nitems, MXBITFILE *mxbitfile);
extern int read_rewind(MXBITFILE *mxbitfile);

#endif /* if !defined(MXBITIO_H_) */
