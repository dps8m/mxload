/*
 * scspell-id: dc9c58c6-ff09-11ee-923f-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "mxbitio.h"
#include "dirsep.h"
#include "mxload.h"

#define MAX_TEMP_FILES 10
static MXBITFILE *tempfiles[MAX_TEMP_FILES] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
static char temppaths[MAX_TEMP_FILES][100];
static char comments[MAX_TEMP_FILES][32];
static int tempfile_open_and_available[MAX_TEMP_FILES] = {0};
static char temp_dir[100]                              = "'";

/* Return an open file in /tmp or directory indicated by TMP environment variable.
 * File is taken from a pool of already open files, if possible. Otherwise it is created and opened. */

MXBITFILE *
get_temp_file(char *mode, char *comment) {
    int i, t;
    char filename[400];
    char *tmp_env_var;

    /* Initialize temp dir name if this is first call */
    if (strcmp(temp_dir, "'") == 0) {
        tmp_env_var = getenv("TMP");
        if (tmp_env_var != NULL && strlen(tmp_env_var) > 0) {
            strcpy(temp_dir, tmp_env_var);
            if (temp_dir[strlen(temp_dir) - 1] != DIR_SEPARATOR)
                strcat(temp_dir, DIR_SEPARATOR_STRING);
        } else {
            strcpy(temp_dir, "/tmp/");
        }
    }

    /* Look for an open and available file */
    for (i = 0; i < MAX_TEMP_FILES; ++i) {
        if (tempfile_open_and_available[i]) {
            tempfile_open_and_available[i] = 0;
            strcpy(comments[i], comment);
            rewind_mxbit_file(tempfiles[i], mode);

            return tempfiles[i];
        }
    }

    /* If none were open and available, pick a free slot in tempfiles and open a new file. */
    for (i = 0; i < MAX_TEMP_FILES; ++i) {
        if (tempfiles[i] == NULL) {
            strcpy(filename, temp_dir);
            strcat(filename, "mxXXXXXX");
            t = mkstemp(filename);
            if (t != -1) {
                close(t);
            } else {
                fprintf(stderr, "Unable to open temporary file: %s.\n", strerror(errno));
                exit(1);
            }
            tempfiles[i] = open_mxbit_file(filename, mode);

            if (tempfiles[i] == NULL)
                perror("Unable to open temporary file.\n");

            strcpy(temppaths[i], filename);
            strcpy(comments[i], comment);

            return tempfiles[i];
        }
    }

    fprintf(stderr, "Unable to open temporary file.\n");
    exit(1);
}

/* Put a temporary file back in the pool */

void
release_temp_file(MXBITFILE *tempfile, char *comment) {
    int i;

    for (i = 0; i < MAX_TEMP_FILES; ++i) {
        if (tempfiles[i] == tempfile) {
            tempfile_open_and_available[i] = 1;

            return;
        }
    }
}

/* Take a temporary file out of the pool and replace it by another, usually to make the original a permanent segment.
 * The file is closed when this call returns and there is no longer any way to get at it by its old name or file pointer. */

void
replace_temp_file(MXBITFILE *tempfile) {
    int i, t;
    MXBITFILE *newtempfile;
    char new_filename[400];

    for (i = 0; i < MAX_TEMP_FILES; ++i) {
        if (tempfiles[i] == tempfile) {
            close_mxbit_file(tempfile);
            strcpy(new_filename, temp_dir);
            strcat(new_filename, "mxXXXXXX");
            t = mkstemp(new_filename);
            if (t != -1) {
                close(t);
            } else {
                fprintf(stderr, "Unable to open temporary file: %s.\n", strerror(errno));
                exit(1);
            }
            newtempfile = open_mxbit_file(new_filename, "wt");
            strcpy(temppaths[i], new_filename);
            memcpy(tempfile, newtempfile, sizeof(MXBITFILE));
            free(newtempfile);

            return;
        }
    }
}

/* Returns the actual pathname of a temporary file, but leaves it in the pool. */

char *
temp_file_name(MXBITFILE *tempfile) {
    int i;

    for (i = 0; i < MAX_TEMP_FILES; ++i)
        if (tempfiles[i] == tempfile)
            return temppaths[i];

    return NULL;
}

/* Delete all remaining temporary files */

void
cleanup_temp_files(void) {
    int i;

    for (i = 0; i < MAX_TEMP_FILES; ++i) {
        if (tempfiles[i] != NULL) {
            close_mxbit_file(tempfiles[i]);
            unlink(temppaths[i]);
            tempfiles[i] = NULL;
        }
    }
}
