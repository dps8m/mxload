/*
 * scspell-id: c100f66c-ff09-11ee-bba8-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !defined(MXLOPTS_H_)
# define MXLOPTS_H_

# define N_ATTRS 5
# define N_FILE_TYPES 7
# define N_LIST_TYPES 3

extern char **file_cv_types[];
extern char **attr_cv_types[];
extern char **list_types[];

struct USER_TRANSLATION {
    char multics_name[24];
    char unix_name[32]; /* Character string representation */
    int id;             /* UID or GID */
    int is_group;       /* As opposed to user/owner */
    struct USER_TRANSLATION *next;
};

struct MXLOPTS {
    struct MXLOPTS *next;
    char path[170];
    char *path_type;
    char new_path[170];
    char *attr_cv_values[N_ATTRS];
    char *file_cv_values[N_FILE_TYPES];
    char *list_values[N_LIST_TYPES];
    char *force_convert;
    char *access;
    char *reload;
    char *dataend;
    int n_files_matched;
    int n_files_loaded;
    struct USER_TRANSLATION *user_translations;
};

#endif /* if !defined(MXLOPTS_H_) */
