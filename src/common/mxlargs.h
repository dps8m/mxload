/*
 * scspell-id: e93542aa-ff09-11ee-b9f0-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !defined(MXLARGS_H_)
# define MXLARGS_H_

# include <stdio.h>

struct MXLARGS {
    unsigned char brief;             /* -b (mxload only) */
    unsigned char verbose;           /* -v */
    unsigned char extremely_verbose; /* -x */
    unsigned char no_map;            /* -n (mxload only )*/
    char *map_filename;              /* -g MAPFILE (mxload only) */
    FILE *map_file;
    unsigned char local_map;         /* -l (mxload only) */
    unsigned char map_only;          /* -m (mxarc only) */
};

extern struct MXLARGS mxlargs;

#endif /* if !defined(MXLARGS_H_) */
