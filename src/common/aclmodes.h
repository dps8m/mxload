/*
 * scspell-id: d0745b84-ff09-11ee-be83-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !defined(ACLMODES_H_)
# define ACLMODES_H_

static char* SEG_ACCESS_MODES[8] = {
    "null", /* 0 */
    "  w ", /* 1 */
    " e  ", /* 2 */
    " ew ", /* 3 */
    "r   ", /* 4 */
    "r w ", /* 5 */
    "re  ", /* 6 */
    "rew "  /* 7 */
};

static char* SHORT_SEG_ACCESS_MODES[8] = {
    "null", /* 0 */
    "w",    /* 1 */
    "e",    /* 2 */
    "ew",   /* 3 */
    "r",    /* 4 */
    "rw",   /* 5 */
    "re",   /* 6 */
    "rew"   /* 7 */
};

static char* DIR_ACCESS_MODES[8] = {
    "null", /* 0 */
    "  a ", /* 1 */
    " m  ", /* 2 */
    " ma ", /* 3 */
    "s   ", /* 4 */
    "s a ", /* 5 */
    "sm  ", /* 6 */
    "sma "  /* 7 */
};

#endif /* if !defined(ACLMODES_H_) */
