/*
 * scspell-id: d28cfc50-ff09-11ee-8296-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* GETOPT */

#include <stdio.h>
#include <string.h>

#include "mxbitio.h"
#include "preamble.h"
#include "mxlopts.h"
#include "optptr.h"
#include "mxload.h"

/* Find a file, directory or subtree control file statement that matches the pathname of the segment read from tape.
 * If found, return a pointer to its retrieval options.  If not found, return NULL. */

struct MXLOPTS *
get_options_ptr(struct PREAMBLE *preamble_ptr, struct MXLOPTS *retrieval_list_ptr) {
    char full_multics_path[170];

    strcpy(full_multics_path, preamble_ptr->dname);
    if (preamble_ptr->ename[0] != '\0') {
        if (full_multics_path[1] != '\0') /* If it's not the ROOT */
            strcat(full_multics_path, ">");

        strcat(full_multics_path, preamble_ptr->ename);
    }

    while (retrieval_list_ptr != NULL) {
        if (strcmp(retrieval_list_ptr->path_type, "file") == 0 && strcmp(retrieval_list_ptr->path, full_multics_path) == 0) {
            return retrieval_list_ptr;
        } else if (strcmp(retrieval_list_ptr->path_type, "directory") == 0 &&
            strcmp(retrieval_list_ptr->path, preamble_ptr->dname) == 0) {
            return retrieval_list_ptr;
        } else if (strcmp(retrieval_list_ptr->path_type, "subtree") == 0 &&
            strncmp(retrieval_list_ptr->path, preamble_ptr->dname, strlen(retrieval_list_ptr->path)) == 0) {
            return retrieval_list_ptr;
        }

        retrieval_list_ptr = retrieval_list_ptr->next;
    }
    return NULL;
}
