/*
 * scspell-id: cb554a3c-ff09-11ee-8859-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !defined(COPYBITS_H_)
# define COPYBITS_H_

# include "mxbitio.h"

extern int copy_to_empty_file(MXBITFILE *infile, MXBITFILE *outfile, long n_bits);
extern int copy_bits(MXBITFILE *infile, MXBITFILE *outfile, long n_bits);
extern void copy_8bit(MXBITFILE *contents_file, char *new_path, unsigned long bitcount);
extern void copy_8bit_to_file(MXBITFILE *contents_file, FILE *outfile, unsigned long bitcount);
extern void copy_file(MXBITFILE *contents_file, char *new_path, int is_ascii, long bit_count);

#endif /* if !defined(COPYBITS_H_) */
