/*
 * scspell-id: a645b88a-ff09-11ee-8240-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#if !defined(MXLOPTSI_H_)
# define MXLOPTSI_H_

char* ascii_cv_values[]            = {"ascii", "8bit", "8bit+9bit", "9bit", "discard", NULL};
char* nonascii_cv_values[]         = {"nonascii", "9bit", "8bit", "8bit+9bit", "discard", NULL};
char* object_cv_values[]           = {"object", "discard", "8bit", "8bit+9bit", "9bit", NULL};
char* ascii_archive_cv_values[]    = {"ascii_archive", "unpack", "8bit", "9bit", "8bit+9bit", "discard", NULL};
char* nonascii_archive_cv_values[] = {"nonascii_archive", "9bit", "unpack", "8bit", "8bit+9bit", "discard", NULL};
char* mbx_cv_values[]              = {"mbx", "9bit", "unpack", "repack", "8bit", "8bit+9bit", "discard", NULL};
char* ms_cv_values[]               = {"ms", "discard", "8bit", "8bit+9bit", "9bit", "unpack", NULL};

char** file_cv_types[] = {ascii_cv_values, nonascii_cv_values, object_cv_values, ascii_archive_cv_values,
    nonascii_archive_cv_values, mbx_cv_values, ms_cv_values, NULL};

char* force_convert_file_cv_types[] = {"force_convert", "8bit", "8bit+9bit", "9bit", "discard", NULL};

char* name_type_cv_values[]   = {"name_type", "BSD", "SysV", "CMS", "MSDOS", NULL};
char* owner_cv_values[]       = {"owner", "author", "bit_count_author", NULL};
char* group_cv_values[]       = {"group", "author", "bit_count_author", NULL};
char* access_time_cv_values[] = {"access_time", "dtu", "now", NULL};
char* mod_time_cv_values[]    = {"modification_time", "dtcm", "now", NULL};

char** attr_cv_types[] = {name_type_cv_values, owner_cv_values, group_cv_values, access_time_cv_values, mod_time_cv_values, NULL};

char* link_list_values[]    = {"link", "none", "global", "local", NULL};
char* acl_list_values[]     = {"acl", "none", "global", "local", NULL};
char* addname_list_values[] = {"addname", "none", "global", "local", NULL};

char** list_types[] = {link_list_values, acl_list_values, addname_list_values, NULL};

#endif /* if !defined(MXLOPTSI_H_) */
