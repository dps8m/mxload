/*
 * scspell-id: 7315e69c-ff09-11ee-b32f-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include "mxlargs.h"
#include "mxbitio.h"

struct MXLARGS mxlargs;

static void copy_8bit(char *contents_filename);

int
main(int argc, char *argv[]) {
    if (argc != 2) {
        (void)fprintf(stderr, "Usage: pr8bit <ninebitfile>\n");
        exit(1);
    }

    copy_8bit(argv[1]);
}

static void
copy_8bit(char *contents_filename) {
    char buffer[2];
    MXBITFILE *infile;
    int n_read;

    /* Open as temporary file so EOF does not cause error message */
    if ((infile = open_mxbit_file(contents_filename, "rt")) == NULL) {
        (void)fprintf(stderr, "Cannot open temp file %s.\n", contents_filename);
        return;
    }

    (void)skip_mxbits(infile, 1L);

    while (1) {
        n_read = get_mxbits(infile, 9, buffer);
        if (n_read != 9) {
            close_mxbit_file(infile);
            return;
        }

        (void)putc(buffer[0], stdout);
    }
}

void
mxlexit(int status) {
    exit(status);
}
