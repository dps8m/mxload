/*
 * scspell-id: 80fcaae8-ff09-11ee-b0c4-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <utime.h>
#if !defined(_AIX)
# include <getopt.h>
#endif /* if !defined(_AIX) */

#include "mxlargs.h"
#include "mxlopts.h"
#include "mxbitio.h"
#include "preamble.h"
#include "mapprint.h"
#include "rdbkrcd.h"
#include "dirsep.h"
#include "tempfile.h"
#include "gettype.h"
#include "cvpath.h"
#include "copybits.h"
#include "parsctl.h"
#include "mxload.h"

static void mxmap_usage(void);
struct MXLARGS mxlargs;

static void
set_attrs(char *new_path, struct MXLOPTS *retrieval_options_ptr, struct PREAMBLE *preamble_ptr,
    struct BRANCH_PREAMBLE *branch_preamble_ptr) {
    char *access_time_action;
    char *mod_time_action;
    char *acl_action;
    char *addname_action;
    struct utimbuf utime_struct;
    char *name_type;
    char *group_action;
    char project_id[20];
    char *owner_action;
    char person_id[20];

    name_type = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "name_type")];

    /* Set the access time if requested */
    access_time_action = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "access_time")];
    if (strcmp(access_time_action, "dtu") == 0)
        utime_struct.actime = branch_preamble_ptr->dtu;
    else
        utime_struct.actime = time(NULL);

    /* Set the modification time if requested */
    mod_time_action = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "modification_time")];
    if (strcmp(mod_time_action, "dtcm") == 0)
        utime_struct.modtime = branch_preamble_ptr->dtm;
    else
        utime_struct.modtime = time(NULL);

    (void)utime(new_path, &utime_struct); /* The actual time-setting */
}

/* Read segment from temp file and write converted form into new path */

static void
convert_seg(MXBITFILE *contents_file, MXBITFILE *preconverted_contents_file, struct PREAMBLE *preamble_ptr,
    struct BRANCH_PREAMBLE *branch_preamble_ptr, char *seg_type, char *conversion_type, struct MXLOPTS *retrieval_options_ptr) {
    char new_path[400];
    char *name_type;
    static char last_dir[170] = "";

    name_type = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "name_type")];
    ++retrieval_options_ptr->n_files_loaded;
    if (make_new_path(preamble_ptr->dname, preamble_ptr->ename, retrieval_options_ptr, "", name_type, new_path) != 0)
        mxlexit(4); /* Make sure original mailbox doesn't get deleted */

    display_conversion_info(new_path, seg_type, conversion_type);
    copy_8bit(contents_file, new_path, preamble_ptr->adjusted_bitcnt);
    set_attrs(new_path, retrieval_options_ptr, preamble_ptr, branch_preamble_ptr);
    return;
}

void
process_seg(MXBITFILE *infile, struct BRANCH_PREAMBLE *branch_preamble_ptr, struct PREAMBLE *preamble_ptr,
    struct MXLOPTS *retrieval_options_ptr, int is_ascii) {
    int seg_has_been_read;
    char *seg_type;
    char *seg_type_by_name;
    char *seg_type_by_content;
    char *conversion_type;
    MXBITFILE *contents_file;

    if (mxlargs.map_only) {
        display_branch_preamble(preamble_ptr, branch_preamble_ptr);

        return;
    }

    contents_file = get_temp_file("wt", "raw file contents");
    if (contents_file == NULL)
        mxlexit(4); /* Make sure original mailbox doesn't get deleted */

    if (rdseg(infile, contents_file, preamble_ptr) != 0)
        goto EXIT;

    display_branch_preamble(preamble_ptr, branch_preamble_ptr);
    convert_seg(contents_file, NULL, preamble_ptr, branch_preamble_ptr, "ascii", "8bit", retrieval_options_ptr);

EXIT:
    release_temp_file(contents_file, "raw file contents");
}

/* This function can be called by mxdearc in place of the usual process_seg function, to process preconverted-to-8bit segments.
 * It must only be called for segment types "ascii" and "ascii_archive" (for the *all important* recursive archive unpacking)
 * when the conversion type for "ascii" will be "8bit" and for "ascii_archive" it will be "unpack". */

void
process_ascii_seg(struct BRANCH_PREAMBLE *branch_preamble_ptr, MXBITFILE *contents_file, struct PREAMBLE *preamble_ptr,
    struct MXLOPTS *retrieval_options_ptr) {
    char *seg_type;
    char *seg_type_by_name;
    char *conversion_type;

    if (mxlargs.map_only) {
        display_branch_preamble(preamble_ptr, branch_preamble_ptr);
        return;
    }

    seg_type_by_name = get_type_by_name(preamble_ptr);
    if (seg_type_by_name != NULL && strcmp(seg_type_by_name, "ascii_archive") == 0) {
        conversion_type = "unpack";
        seg_type        = "ascii_archive";
    } else {
        conversion_type = "8bit";
        seg_type        = "ascii";
    }

    if (strcmp(conversion_type, "unpack") != 0)
        display_branch_preamble(preamble_ptr, branch_preamble_ptr);

    convert_seg(NULL, contents_file, preamble_ptr, branch_preamble_ptr, seg_type, conversion_type, retrieval_options_ptr);
}

int
main(int argc, char *argv[]) {
    MXBITFILE *infile;
    char *temp_path;
    struct PREAMBLE preamble;
    struct BRANCH_PREAMBLE branch_preamble;
    struct DIRLIST_PREAMBLE dirlist_preamble;
    int i;
    int option_letter;
    MXBITFILE *preamble_file;

    (void)memset(&mxlargs, 0, sizeof(mxlargs));
    while ((option_letter = getopt(argc, argv, "vx")) != EOF) {
        switch (option_letter) {
            case 'v': mxlargs.verbose = 1; break;           /* Verbose: long listing */

            case 'x': mxlargs.extremely_verbose = 1; break; /* Extremely verbose: full listing */

            case '?': mxmap_usage(); mxlexit(4);            /* ? or unrecognized option */
        }
    }

    mxlargs.map_file = stdout;

    /* Adjust arguments to skip over program name and options */
    argc -= optind;
    argv += optind;

    if (argc != 1)
        mxmap_usage();

    if ((infile = open_mxbit_file(argv[0], "r")) == NULL) {
        (void)fprintf(stderr, "Cannot open Multics backup file %s.\n", argv[0]);
        exit(1);
    }

    preamble_file = get_temp_file("wt", "preamble");

    while (rdbkrcd(infile, preamble_file, &preamble) == 0 && rdseg(infile, NULL, &preamble) == 0) {
        if (preamble.record_type == SEGMENT_RECORD) {
            (void)get_branch_preamble(preamble_file, &branch_preamble, &preamble);
            display_branch_preamble(&preamble, &branch_preamble);
            if (branch_preamble.addnames != NULL)
                free(branch_preamble.addnames);

            if (branch_preamble.acl != NULL)
                free((char *)branch_preamble.acl);
        } else if (preamble.record_type == DIRECTORY_RECORD) {
            (void)get_branch_preamble(preamble_file, &branch_preamble, &preamble);
            display_branch_preamble(&preamble, &branch_preamble);
            if (branch_preamble.addnames != NULL)
                free(branch_preamble.addnames);

            if (branch_preamble.acl != NULL)
                free((char *)branch_preamble.acl);
        } else if (preamble.record_type == DIRLIST_RECORD) {
            (void)get_dirlist_preamble(preamble_file, &dirlist_preamble, &preamble);
            display_dirlist_preamble(&preamble, &dirlist_preamble);
            for (i = 0; i < dirlist_preamble.nlinks; ++i)
                if ((dirlist_preamble.links + i)->addnames != NULL)
                    free((dirlist_preamble.links + i)->addnames);

            free((char *)dirlist_preamble.links);
        }
    }

    mxlexit(0);
}

void
mxlexit(int status) {
    cleanup_temp_files();
    exit(status);
}

/* Print usage message */

static void
mxmap_usage(void) {
    (void)fprintf(stderr, "Usage: mxmap [-options] <dump_file>\n");
    (void)fprintf(stderr, "\nOptions are:\n");
    (void)fprintf(stderr, "-v\t\t= verbose; produce long map\n");
    (void)fprintf(stderr, "-x\t\t= extremely verbose; produce full map\n");

    mxlexit(4);
}
