/*
 * scspell-id: 89b6f71a-ff09-11ee-81bf-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* MXMBX */

/* Read Multics mailboxes and retrieve messages. This is basically a
 * lobotomized version of mxarc. */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <utime.h>
#include <signal.h>
#include <sys/stat.h>
#if !defined(_AIX)
# include <getopt.h>
#endif /* if !defined(_AIX) */

#include "mxlargs.h"
#include "mxbitio.h"
#include "preamble.h"
#include "parsctl.h"
#include "mxlopts.h"
#include "rdbkrcd.h"
#include "cvpath.h"
#include "dirsep.h"
#include "optptr.h"
#include "gettype.h"
#include "mapprint.h"
#include "copybits.h"
#include "mxmseg.h"
#include "aclmodes.h"
#include "tempfile.h"
#include "mxload.h"

static void usage(void);
static void onintr(int);
static void convert_seg(MXBITFILE *contents_filename, MXBITFILE *preconverted_contents_file, struct PREAMBLE *preamble_ptr,
    struct BRANCH_PREAMBLE *branch_preamble_ptr, char *seg_type, char *conversion_type, struct MXLOPTS *retrieval_options_ptr);
static void set_attrs(char *new_path, struct MXLOPTS *retrieval_options_ptr, struct PREAMBLE *preamble_ptr,
    struct BRANCH_PREAMBLE *branch_preamble_ptr);
struct MXLARGS mxlargs;

static char renamed_mbx[400] = "";

int
main(int argc, char *argv[]) {
    char *temp_path;
    struct PREAMBLE preamble;
    struct BRANCH_PREAMBLE branch_preamble;
    struct DIRLIST_PREAMBLE dirlist_preamble;
    struct MXLOPTS *retrieval_options_ptr;
    int option_letter;
    char *control_files[50];
    int n_control_files;
    int unpack_sw;
    int table_sw;
    int repack_sw;
    int extract_sw;
    MXBITFILE *mbx_file;
    int is_ascii;
    static char mbx_header_sentinel[4] = {0x55, 0x55, 0x55, 0x55};
    char mbx_header[10];
    int i;
    int n_mbxs;
    int mxmseg_result;
    struct stat statbuf;
    int rename_result;

    if (signal(SIGINT, SIG_IGN) != SIG_IGN)
        (void)signal(SIGINT, onintr);

    n_control_files  = 0;
    control_files[0] = NULL;
    (void)memset(&mxlargs, 0, sizeof(mxlargs));
    mxlargs.map_file     = stdout;
    mxlargs.map_filename = NULL;
    unpack_sw = table_sw = extract_sw = repack_sw = 0;
    while ((option_letter = getopt(argc, argv, "c:ntrux")) != EOF) {
        switch (option_letter) {
            case 'c': control_files[n_control_files++] = strcpy(malloc(strlen(optarg) + 1), optarg); break; /* Control file */

            case 't':
                table_sw         = 1;
                mxlargs.map_only = 1;
                break;                           /* Map only */

            case 'n': mxlargs.no_map = 1; break; /* No map */

            case 'r': repack_sw = 1; break;      /* Repack into stream mailbox */

            case 'u': unpack_sw = 1; break;      /* Unpack */

            case 'x': extract_sw = 1; break;     /* Extract */

            case '?': usage(); mxlexit(4);       /* ? or unrecognized option */
        }
    }

    if (unpack_sw + table_sw + extract_sw + repack_sw != 1)
        usage();

    if (mxlargs.no_map)
        mxlargs.map_file = fopen("/dev/null", "w");

    /* Adjust arguments to skip over program name and options, putting position at mailbox file name */
    argc -= optind;
    argv += optind;

    if (argc < 1)
        usage();

    n_mbxs = argc;

    if (table_sw) {
        retrieval_options_ptr = NULL;
    } else {
        /* Get retrieval options.  Returns default options only, i.e. builtin
         * defaults plus result of any global statements in control files. */
        retrieval_options_ptr = parsctl(n_control_files, control_files, 0, NULL, 1);
        /* Adjust the retrieval options a bit. */
        if (extract_sw || repack_sw)
            retrieval_options_ptr->reload = "flat";

        if (repack_sw)
            retrieval_options_ptr->path_type = "file";
    }

    /* Construct the nullest of preambles */
    (void)memset(&preamble, 0, sizeof(preamble));
    preamble.record_type = SEGMENT_RECORD;
    (void)memset(&branch_preamble, 0, sizeof(branch_preamble));

    for (i = 0; i < n_mbxs; ++i) {
        if ((mbx_file = open_mxbit_file(argv[i], "rt")) == NULL) {
            (void)fprintf(stderr, "Cannot open Multics mailbox file %s.\n", argv[i]);
            continue;
        }

        if (stat(argv[i], &statbuf) == 0) {
            branch_preamble.dtu = statbuf.st_atime;
            branch_preamble.dtm = branch_preamble.dtbm = statbuf.st_mtime;
        }

        /* Read what should be header of mailbox. */
        (void)get_mxbits(mbx_file, 72L, mbx_header);
        if (memcmp(mbx_header + 5, mbx_header_sentinel, 4) != 0) {
            (void)fprintf(stderr, "mxmbx:        %s does not appear to be a Multics mailbox.\n", argv[i]);
            (void)fprintf(stderr, "Be sure that the mailbox was reloaded ");
            (void)fprintf(stderr, "with 9bit conversion.\n");
            continue;
        }

        if (i > 0)
            (void)fprintf(mxlargs.map_file, "\n");

        if (n_mbxs > 1)
            (void)fprintf(mxlargs.map_file, "%s\n", argv[i]);

        if (unpack_sw || repack_sw) {
            /* Rename the mailbox file to make way for a new directory or file before unpacking or repacking. */
            (void)cvpath(argv[i], "", "BSD", renamed_mbx);
            rename_result = rename(argv[i], renamed_mbx);
            if (rename_result != 0) {
                (void)fprintf(stderr, "Cannot rename %s to %s.\n", argv[i], renamed_mbx);
                continue;
            }

            /* Put results in directory or file with old mailbox name */
            (void)strcpy(retrieval_options_ptr->new_path, argv[i]);
        }

        mxmseg_result = mxmseg(mbx_file, &preamble, retrieval_options_ptr, &branch_preamble, repack_sw, "mbx");
        /* When unpacking or repacking, delete the original mailbox */
        if ((unpack_sw || repack_sw) && rename_result == 0) {
            if (mxmseg_result == 0)
                (void)unlink(renamed_mbx);
            else
                (void)fprintf(stderr, "Original mailbox is in %s\n", renamed_mbx);

            renamed_mbx[0] = '\0';
        }
    }

    mxlexit(0);
}

/* Print usage message */

static void
usage(void) {
    (void)fprintf(stderr, "Usage: mxmbx -t [options] <mailbox_file> ...\n");
    (void)fprintf(stderr, "Or:    mxmbx -u [options] <mailbox_file> ...\n");
    (void)fprintf(stderr, "Or:    mxmbx -r [options] <mailbox_file> ...\n");
    (void)fprintf(stderr, "Or:    mxmbx -x [options] <mailbox_file> ...\n");
    (void)fprintf(stderr, "\nOptions are:\n");
    (void)fprintf(stderr, "-c control_file\t= control file; may be specified more than once\n");
    (void)fprintf(stderr, "-n\t\t= no messages list\n");
    (void)fprintf(stderr, "-t\t\t= table; just produce a message list\n");
    (void)fprintf(stderr, "-u\t\t= unpack; replace mailbox by ");
    (void)fprintf(stderr, "directory containing messages\n");
    (void)fprintf(stderr, "-r\t\t= repack; replace mailbox by ");
    (void)fprintf(stderr, "file containing messages\n");
    (void)fprintf(stderr, "-x\t\t= extract; put copies of messages in current directory\n");

    mxlexit(4);
}

static void
onintr(int sig) {
    mxlexit(4);
}

/* Exit after cleaning up.  Called by various subrs. */

void
mxlexit(int status) {
    if (renamed_mbx[0] != '\0')
        (void)fprintf(stderr, "Original mailbox is in %s\n", renamed_mbx);

    cleanup_temp_files();
    exit(status);
}

void
process_seg(MXBITFILE *infile, struct BRANCH_PREAMBLE *branch_preamble_ptr, struct PREAMBLE *preamble_ptr,
    struct MXLOPTS *retrieval_options_ptr, int is_ascii) {
    int seg_has_been_read;
    char *seg_type;
    char *seg_type_by_name;
    char *seg_type_by_content;
    char *conversion_type;
    MXBITFILE *contents_file;

    if (mxlargs.map_only) {
        display_branch_preamble(preamble_ptr, branch_preamble_ptr);
        return;
    }

    contents_file = get_temp_file("wt", "raw file contents");
    if (contents_file == NULL)
        mxlexit(4); /* Make sure original mailbox doesn't get deleted */

    if (rdseg(infile, contents_file, preamble_ptr) != 0)
        goto EXIT;

    display_branch_preamble(preamble_ptr, branch_preamble_ptr);
    convert_seg(contents_file, NULL, preamble_ptr, branch_preamble_ptr, "ascii", "8bit", retrieval_options_ptr);
EXIT:
    release_temp_file(contents_file, "raw file contents");
}

/* This function can be called by mxdearc in place of the usual process_seg function, to process preconverted-to-8bit segments.
 * It must only be called for segment types "ascii" and "ascii_archive" (for the *all important* recursive archive unpacking)
 * when the conversion type for "ascii" will be "8bit" and for "ascii_archive" it will be "unpack". */

void
process_ascii_seg(struct BRANCH_PREAMBLE *branch_preamble_ptr, MXBITFILE *contents_file, struct PREAMBLE *preamble_ptr,
    struct MXLOPTS *retrieval_options_ptr) {
    char *seg_type;
    char *seg_type_by_name;
    char *conversion_type;

    if (mxlargs.map_only) {
        display_branch_preamble(preamble_ptr, branch_preamble_ptr);
        return;
    }

    seg_type_by_name = get_type_by_name(preamble_ptr);
    if (seg_type_by_name != NULL && strcmp(seg_type_by_name, "ascii_archive") == 0) {
        conversion_type = "unpack";
        seg_type        = "ascii_archive";
    } else {
        conversion_type = "8bit";
        seg_type        = "ascii";
    }

    if (strcmp(conversion_type, "unpack") != 0)
        display_branch_preamble(preamble_ptr, branch_preamble_ptr);

    convert_seg(NULL, contents_file, preamble_ptr, branch_preamble_ptr, seg_type, conversion_type, retrieval_options_ptr);
}

/* Read segment from temp file and write converted form into new path */

static void
convert_seg(MXBITFILE *contents_file, MXBITFILE *preconverted_contents_file, struct PREAMBLE *preamble_ptr,
    struct BRANCH_PREAMBLE *branch_preamble_ptr, char *seg_type, char *conversion_type, struct MXLOPTS *retrieval_options_ptr) {
    char new_path[400];
    char *name_type;
    static char last_dir[170] = "";

    name_type = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "name_type")];
    ++retrieval_options_ptr->n_files_loaded;
    if (make_new_path(preamble_ptr->dname, preamble_ptr->ename, retrieval_options_ptr, "", name_type, new_path) != 0)
        mxlexit(4); /* Make sure original mailbox doesn't get deleted */

    display_conversion_info(new_path, seg_type, conversion_type);
    copy_8bit(contents_file, new_path, preamble_ptr->adjusted_bitcnt);
    set_attrs(new_path, retrieval_options_ptr, preamble_ptr, branch_preamble_ptr);

    return;
}

static void
set_attrs(char *new_path, struct MXLOPTS *retrieval_options_ptr, struct PREAMBLE *preamble_ptr,
    struct BRANCH_PREAMBLE *branch_preamble_ptr) {
    char *access_time_action;
    char *mod_time_action;
    char *acl_action;
    char *addname_action;
    struct utimbuf utime_struct;
    char *name_type;
    char *group_action;
    char project_id[20];
    char *owner_action;
    char person_id[20];

    name_type = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "name_type")];

    /* Set the access time if requested */
    access_time_action = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "access_time")];
    if (strcmp(access_time_action, "dtu") == 0)
        utime_struct.actime = branch_preamble_ptr->dtu;
    else
        utime_struct.actime = time(NULL);

    /* Set the modification time if requested */
    mod_time_action = retrieval_options_ptr->attr_cv_values[get_keyword_values_index(attr_cv_types, "modification_time")];
    if (strcmp(mod_time_action, "dtcm") == 0)
        utime_struct.modtime = branch_preamble_ptr->dtm;
    else
        utime_struct.modtime = time(NULL);

    (void)utime(new_path, &utime_struct); /* The actual time-setting */
}
