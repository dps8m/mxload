/*
 * scspell-id: 9031e3d4-ff09-11ee-ad17-80ee73e9b8e7
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 1987-2015 Oxford Systems, Inc.
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* MXASCI */

/* Read a Multics file and translate 9-bit ASCII to 8-bit */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <utime.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "mxlargs.h"
#include "preamble.h"
#include "mxlopts.h"
#include "copybits.h"
#include "mxbitio.h"
#include "cvpath.h"
#include "gettype.h"
#include "tempfile.h"
#include "mapprint.h"
#include "rdbkrcd.h"
#include "parsctl.h"
#include "mxload.h"

struct MXLARGS mxlargs;

int
main(int argc, char *argv[]) {
    MXBITFILE *ninebit_file;
    FILE *f;
    struct stat statbuf;
    long bitcount;

    if (argc < 2 || argc > 3) {
        (void)fprintf(stderr, "Usage: mxascii <ninebitfile> [eightbitfile]\n");
        exit(1);
    }

    if ((ninebit_file = open_mxbit_file(argv[1], "rt")) == NULL) {
        (void)fprintf(stderr, "Cannot open %s.\n", argv[1]);
        exit(1);
    }

    stat(argv[1], &statbuf);

    bitcount = statbuf.st_size * 8L;
    bitcount -= bitcount % 9;

    if (argc == 2) {
        copy_8bit_to_file(ninebit_file, stdout, bitcount);
    } else {
        if ((f = fopen(argv[2], "r")) != NULL) {
            (void)fprintf(stderr, "%s already exists.\n", argv[2]);
            exit(1);
        }

        if (f)
            (void)fclose(f);

        copy_8bit(ninebit_file, argv[2], bitcount);
    }
}

/* Exit after cleaning up.  Called by various subrs. */

void
mxlexit(int status) {
    exit(status);
}
