#!/bin/sh

################################################################################
#
# scspell-id: 1ba6e324-ff0a-11ee-8cfa-80ee73e9b8e7
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2018-2024 Ryan M. Lederman <lederman@gmail.com>
# Copyright (c) 2018-2024 Jeffrey H. Johnson <trnsz@pobox.com>
# Copyright (c) 2023-2024 The DPS8M Development Team
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
################################################################################

################################################################################
# Check for csh as sh

# shellcheck disable=SC2006,SC2046,SC2065,SC2116
test _`echo asdf 2>/dev/null` != _asdf >/dev/null &&\
    printf '%s\n' "Error: csh is not supported." &&\
    exit 1

################################################################################
# Check directory

test -f "./${0##*/}" > /dev/null 2>&1 \
  || {
    printf '%s\n' "Error: cannot locate script in current directory."
    exit 1
  }

################################################################################
# Fail early on pipelines, if possible

# shellcheck disable=SC3040
(set -o pipefail > /dev/null 2>&1) && set -o pipefail > /dev/null 2>&1

################################################################################
# Find tool

find_tool()
{
  command -v "${1:-}" > /dev/null 2>&1 \
    || {
      printf 'Error: Could not locate %s.\n' "${1:-}"
      exit 1
    }
}

################################################################################
# Editorconfig-checker

run_editorconfig_checker()
{
  printf '%s' "Checking with editorconfig-checker ..."
  editorconfig-checker && printf ' %s\n' "complete."
}

################################################################################
# Cppi (and expand)

run_cppi()
{
  printf '%s' "Formatting with cppi ... "
  ( find . -name "*.[ch]" -o -name "*.cc" -o -name "*.hh" \
      | grep -Ev '(build/.*|\.git/.*)' | xargs -I{} "${SHELL:-sh}" -c \
          'set -e; cppi "{}" | expand > "{}.cppi" && mv -f "{}.cppi" "{}"'
  ) && printf ' %s\n' "complete."
}

################################################################################
# Clang-format

run_clang_format()
{
  printf '%s' "Formatting with clang-format ..."
  ( find . -name "*.[ch]" -o -name "*.cc" -o -name "*.hh" \
      | grep -Ev '(build/.*|\.git/.*)' | xargs -I{} "${SHELL:-sh}" -c \
          'set -e; clang-format -i "{}"'
  ) && printf ' %s\n' "complete."
}

################################################################################
# Find tools

find_tool clang-format
find_tool expand
find_tool cppi
find_tool editorconfig-checker

################################################################################
# Run tools

run_clang_format
run_cppi
run_editorconfig_checker

################################################################################
