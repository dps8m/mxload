<!-- scspell-id: 8954aa78-ff0a-11ee-93ec-80ee73e9b8e7                       -->
<!-- SPDX-License-Identifier: MIT                                           -->
<!--                                                                        -->
<!-- Copyright (c) 1987-2015 Oxford Systems, Inc.                           -->
<!-- Copyright (c) 2023-2024 The DPS8M Development Team                     -->
<!--                                                                        -->
<!-- Permission is hereby granted, free of charge, to any person            -->
<!-- obtaining a copy of this software and associated documentation files   -->
<!-- (the "Software"), to deal in the Software without restriction,         -->
<!-- including without limitation the rights to use, copy, modify, merge,   -->
<!-- publish, distribute, sublicense, and/or sell copies of the Software,   -->
<!-- and to permit persons to whom the Software is furnished to do so,      -->
<!-- subject to the following conditions:                                   -->
<!--                                                                        -->
<!-- The above copyright notice and this permission notice shall be         -->
<!-- included in all copies or substantial portions of the Software.        -->
<!--                                                                        -->
<!-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        -->
<!-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     -->
<!-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. -->
<!-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   -->
<!-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   -->
<!-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      -->
<!-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 -->

# mxload

## Overview

* **mxload** is a suite of software that can read
  [Multics standard-format tapes](https://multicians.org/mspm-bb-3-01.html),
  reload data, produce maps, unpack
  [archives](https://multicians.org/mga.html#archive),
  [Forum](https://multicians.org/mgf.html#forum) meetings, message
  segments, and [mailboxes](https://multicians.org/mgm.html#mailbox).

* Segments that contain only ASCII characters are, by default, converted to
  8-bit format when reloaded with the `mxload` command.

* Segments that have high bits set in any byte, or that are in one of the
  recognized formats (mailbox, forum meeting, etc.) are reloaded in a packed
  9-bit format that can be further processed with one of the other commands
  (`mxarc`, `mxascii`, `mxmbx`, or `mxforum`).

## Notes

* The [`tap2raw`](https://gitlab.com/dps8m/tap2raw) utility can be used to
  convert *`tap`* files produced by the
  [DPS8M Simulator](https://dps8m.gitlab.io) to the required
  *`raw`* format (*i.e.* `tap2raw < input.tap > output.raw`).

## Build

### Software

* Run **`make`** (or **`gmake`**) to build the software.
  * [GNU Make](https://www.gnu.org/software/make/) version 3.81 (or later)
    and a C compiler is required to build **mxload**.
    * Standard build variables (*e.g.* `CC`, `CFLAGS`, `LDFLAGS`, etc.) are
      respected.
    * VM/CMS and MS-DOS builds are no longer supported.

### Documentation

* Run **`make docs`** (or **`gmake docs`**) to build the PDF documentation.
  * Building the documentation requires
    [GNU roff](https://www.gnu.org/software/groff/) (`groff`) and
    [Ghostscript](https://www.ghostscript.com/) (`ps2pdf`).

## Acknowledgements

* [**Jim Homan**](https://multicians.org/multicians.html#Homan) and
  [**Olin Sibert**](https://multicians.org/multicians.html#Sibert), for
  creating the **mxload** product.
* **Olin Sibert**, *Oxford Systems, Inc.*, for allowing **mxload** to be
  relicensed and distributed under an
  [**Open Source Initiative** approved](https://opensource.org/license/mit)
  and [**Blue Oak Council** certified](https://blueoakcouncil.org/list#MIT)
  permissive open-source license.

## License

* **mxload** is distributed under the terms of the [**MIT License**](LICENSE).

## See also

* [Old **mxload** homepage](https://multicians.org/mxload.html)
  ([*multicians.org*](https://multicians.org/))
* [*mxload: Read Multics backup tapes on UNIX*
  ](https://multicians.org/mxload/doc/paper.pdf) (*PDF*)
* [*A portable package for reading Multics backup tapes*
  ](https://multicians.org/mxload/doc/hlsua.pdf) (*PDF*)
* [dps8m/**tap2raw**](https://gitlab.com/dps8m/tap2raw)
* [dps8m/**dps8m_devel_tools**](https://gitlab.com/dps8m/dps8m_devel_tools)
